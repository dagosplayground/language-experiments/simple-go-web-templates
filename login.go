package main

import (
	// "fmt"
	"html/template"
	"log"
	"net/http"
	// "os"
)

func loginHandler(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/login" {
		http.Error(w, "404 not found", http.StatusNotFound)
		return
	}
	if r.Method != "GET" {
		http.Error(w, "method is not supported", http.StatusNotFound)
		return
	}

	tmpl, tmplErr := template.ParseFiles("templates/ht.html")
	if tmplErr != nil {
		log.Fatal(tmplErr)
	}
	data := HeaderTemplate{
		HomeActive:    false,
		DashActive:    false,
		AppActive:     false,
		LoginActive:   true,
		BodyParagraph: "Login string.",
	}

	errEx := tmpl.Execute(w, data)
	if errEx != nil {
		log.Fatal(errEx)
	}
}

package main

import (
	// "fmt"
	"html/template"
	"log"
	"net/http"
	// "os"
)

func dashboardHandler(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/dashboard" {
		http.Error(w, "404 not found", http.StatusNotFound)
		return
	}
	if r.Method != "GET" {
		http.Error(w, "method is not supported", http.StatusNotFound)
		return
	}

	tmpl, tmplErr := template.ParseFiles("templates/ht.html")
	if tmplErr != nil {
		log.Fatal(tmplErr)
	}
	data := HeaderTemplate{
		HomeActive:    false,
		DashActive:    true,
		AppActive:     false,
		LoginActive:   false,
		BodyParagraph: "Dashboard string.",
	}

	errEx := tmpl.Execute(w, data)
	if errEx != nil {
		log.Fatal(errEx)
	}
}

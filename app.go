package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
	"io/ioutil"
	// "os"
)

func libD3arcHandler(w http.ResponseWriter, r *http.Request){
	jslib, errLib := ioutil.ReadFile("libs/d3arc.js")
	if errLib != nil {
		fmt.Println("Error opening d3arc.js")
		log.Fatal(errLib)
	}

	fmt.Fprintf(w, string(jslib))
}

func appHandler(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/app" {
		http.Error(w, "404 not found", http.StatusNotFound)
		return
	}
	if r.Method != "GET" {
		http.Error(w, "method is not supported", http.StatusNotFound)
		return
	}

	tmpl, tmplErr := template.ParseFiles("templates/ht.html")
	if tmplErr != nil {
		log.Fatal(tmplErr)
	}
	data := HeaderTemplate{
		HomeActive:    false,
		DashActive:    false,
		AppActive:     true,
		LoginActive:   false,
		BodyParagraph: "App string.",
	}

	errEx := tmpl.Execute(w, data)
	if errEx != nil {
		log.Fatal(errEx)
	}
}

[[_TOC_]]

# Template Base

Simple template base webserver writtin Go. 

# Build

Use build script. 

# Launch

Simple to launch. `./selfserv 8080` to start and set port. 

Notice, Apps tab has a chart that is interactive.

var innerRad = 80;
var outerRad = 100;

var CenterX = 0,
	CenterY = 0,
	TopX,
	TopY,
	LeftX,
	LeftY,
	RightX,
	RightY;

var svg, arcgreen, arcred, arcyellow, needleG;
var transitionMs = 750;

class SmallGauge {
	// Basic Gauge Chart, must be a svg to match on ID	

	constructor(input) {
		this.id = input.id; // Must be an ID and not a class of a SVG
		this.innerRadius = input.innerRadius || innerRad;
		this.outerRadius = input.outerRadius || outerRad;
		this.NeedleHeight = this.outerRadius;
		this.NeedleRadius = input.NeedleRadius || 3;
		this.margin = { "Left": 1, "Right": 1, "Top": 1, "Bottom": 1 };
	if (input.margin != undefined) {
		this.margin.Left = input.margin.Left || 1;
		this.margin.Right = input.margin.Right || 1;
		this.margin.Top = input.margin.Top || 1;
		this.margin.Bottom = input.margin.Bottom || 1;
	}
	  this.transitionMs = input.transitionMs || 750;
		this.theta = input.theta || 0;
	}

CreateSmallGauge(){

	// svg = d3.selectAll("#"+this.id)
	svg = d3.select("#"+this.id)
		.append("g")
		.attr("transform", "translate(150,100)");
	
	// An arc will be created
	arcgreen = d3.arc()
		.innerRadius(this.innerRadius)
		.outerRadius(this.outerRadius)
		.startAngle(Math.PI*2/12)
		.endAngle(Math.PI/2);
	
	arcyellow = d3.arc()
		.innerRadius(this.innerRadius)
		.outerRadius(this.outerRadius)
		.startAngle(-Math.PI/6)
		.endAngle(Math.PI/6);
	
	arcred = d3.arc()
		.innerRadius(this.innerRadius)
		.outerRadius(this.outerRadius)
		.startAngle(-Math.PI/2)
		.endAngle(-Math.PI/6);
	
	svg.append("path")
		.attr("class", "arc")
		.attr("d", arcgreen)
		.attr("fill","green");
	svg.append("path")
		.attr("class", "arc")
		.attr("d", arcyellow)
		.attr("fill","yellow");
	svg.append("path")
		.attr("class", "arc")
		.attr("d", arcred)
		.attr("fill","red");

	// Create Needle
	needleG = svg.append("g")
		.attr("tranform", "translate(" + (this.margin.Left + this.outerRadius) + "," + (this.margin.Top + this.outerRadius + this.NeedleRadius) + ")")
		.attr('class', this.id+'_needle');
	
	var thetaRad = percToRad(0) / 2;
	self.theta = thetaRad
	TopX = CenterX - this.NeedleHeight * Math.cos(thetaRad);
	TopY = CenterY - this.NeedleHeight * Math.sin(thetaRad);

	LeftX = CenterX - this.NeedleRadius * Math.cos(thetaRad - Math.PI / 2);
	LeftY = CenterY - this.NeedleRadius * Math.sin(thetaRad - Math.PI / 2);

	RightX = CenterX - this.NeedleRadius * Math.cos(thetaRad + Math.PI / 2);
	RightY = CenterY - this.NeedleRadius * Math.sin(thetaRad + Math.PI / 2);

	needleG.append("path").attr('d', "M " + LeftX + ' ' + LeftY + ' L ' + TopX + ' ' + TopY + ' L ' + RightX + ' ' + RightY);

	needleG.append("circle").attr("r", this.NeedleRadius+2).attr("fill", this.NeedleColor);
	}

    // Give a percentage and the needle will move
	updateGaugeChart(percent) {
		var thetaRad = percToRad(percent) / 2; 
	  
		var moveNeedle = d3.select('.'+this.id+'_needle')
		moveNeedle.transition()
			.select("path")
			.duration(transitionMs)
			.ease(d3.easeElastic)
			.attr('transform', 'rotate(' + percToGauge(percent) + ')');

		self.theta = thetaRad;
	}

}

percToGauge = function (perc) {
	return perc * 180;
}

percToDeg = function (perc) {
	return perc * 360;
};

percToRad = function (perc) {
	return degToRad(percToDeg(perc));
};

degToRad = function (deg) {
	return deg * Math.PI / 180;
};

